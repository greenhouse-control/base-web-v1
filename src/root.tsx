import React from 'react';
import './root.css';
import 'antd/dist/antd.css';
import AppWrapper from './components/app_wrapper/page';

export default class RootApp extends React.Component {

    render() {
        return (
            <AppWrapper />
        );
    }
}
