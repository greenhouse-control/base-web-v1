import React, { Component } from 'react';
import {DataProfile} from '../../../access';
import { Descriptions } from 'antd';
import 'antd/dist/antd.css';

type TypeOfData = {
    time: string,
    temperature: string,
    humidity: string,
    lux: string,
    isLoading: boolean
}
export default class SensorsNow extends Component<{},TypeOfData> {
    constructor(props: any) {
        super(props);
        this.state = {
            time: '',
            temperature: '',
            humidity: '',
            lux: '',
            isLoading: false
        };
    }

    componentDidMount(){
        
        const API_SENSORS_NOW = 'sensors-now';
        const getData = async() =>  {
            try {
                this.setState({isLoading: true});
                const resp =  await DataProfile.Get(API_SENSORS_NOW);
                this.setState({
                    time: resp.data['0']['time'],
                    temperature: resp.data['0']['temperature'],
                    humidity: resp.data['0']['humidity'],
                    lux: resp.data['0']['lux'],
                    });
            } catch(error) {
                console.log('Error', error);
            } finally {
                this.setState({isLoading: false});
            }
           
        }
        setInterval(() => {getData()}, 5000);
            
            
    }
    render(){
        return(
            <div style={{width: '50%'}}>
                <Descriptions title="Sensors Now" layout="vertical" bordered column={{sm:4}} >
                    <Descriptions.Item label="Time">{this.state.time}</Descriptions.Item>
                    <Descriptions.Item label="Temperature">{this.state.temperature}</Descriptions.Item>
                    <Descriptions.Item label="Humidity">{this.state.humidity}</Descriptions.Item>
                    <Descriptions.Item label="Lux">{this.state.lux}</Descriptions.Item>
                </Descriptions>
            </div>
        )
    }

}