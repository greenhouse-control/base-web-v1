import React, { Component } from 'react';
import { Layout } from 'antd';
import SideBarHome from '../side_bar_home';
import ContentHome from '../content_home';

export default class Home extends Component<{ match: any }, {}> {
    state = {
        collapsed: false,
    };

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };

    render() {
        return (
            <div>
                <Layout>
                    <SideBarHome match={this.props.match}/>
                    <ContentHome match={this.props.match}/>
                </Layout>
            </div>
        );
    }
}