import React, { Component } from 'react';
import { Layout, Menu, Icon } from 'antd';
import { Link } from "react-router-dom";
const { Sider } = Layout;


export default class SideBarHome extends Component<{ match: any },{}> {
    state = {
        collapsed: false,
    };

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };
    render() {
        return (
            
            <Sider theme="light" trigger={null} collapsible collapsed={this.state.collapsed}>
                <Menu theme="light" mode="inline" defaultSelectedKeys={['1']}>
                    <Menu.Item style={{ color: '#135200' }}>
                        <Icon
                            className="trigger"
                            type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                            onClick={this.toggle}
                        />
                    </Menu.Item>

                    <Menu.Item key="1">
                        <Link to={`${this.props.match.url}/monitoring`} style={{ color: '#096dd9' }}>
                            <Icon type="desktop" />
                            <span>Monitoring</span>
                        </Link>
                    </Menu.Item>

                    <Menu.Item key="2">
                        <Link to={`${this.props.match.url}/controlling-switch`} style={{ color: '#08979c' }}>
                            <Icon type="control" />
                            <span>Controlling Switch</span>
                        </Link>
                    </Menu.Item>

                    <Menu.Item key="3">
                        <Link to={`${this.props.match.url}/setting`} style={{ color: '#531dab' }}>
                            <Icon type="setting" />
                            <span>Setting</span>
                        </Link>
                    </Menu.Item>
                </Menu>
            </Sider>
            
        )
    }

}
