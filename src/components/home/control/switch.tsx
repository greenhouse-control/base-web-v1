import React, { Component } from 'react';
import { Switch, Button, Popconfirm, message, } from 'antd';
// import {DataProfile} from '../../access';

type TypeOfSwitch = {
    relay1: boolean,
    isLock: boolean
}
export default class SwitchControl extends Component <{},TypeOfSwitch> {
    constructor(props: any) {
        super(props);
        this.state = {
            isLock: true,        
            relay1 : false
        };
	} 
	

    toggle = () => {
        this.setState({
          isLock: !this.state.isLock,
        });
      };
    
    onChange = (relay1: any) => {
        // relay1 ? DataProfile.Post('switch/relay1/on'): DataProfile.Post('switch/relay1/off')
        console.log(`switch to ${relay1}`);
      }
    
    text = 'Are you sure to delete this task?';

    confirm = () => {
        message.info('Clicked on Yes.');
    }

    render() {
        return (
          <div>
            <br />
            <span>Relay 1</span>
                <div>123</div>

                <Popconfirm placement="topLeft" title={this.text} onConfirm={this.confirm} okText="Yes" cancelText="No">
                    <Button type="primary" onClick={this.toggle}>
                        {this.state.isLock ? 'Locked Switch' : 'Unlocked Switch'}
                    </Button>    
                </Popconfirm>
                <Switch 
                    disabled={this.state.isLock} 
                    onChange={this.onChange} 
                    defaultChecked={false}
                    
                />

               

                
            
            
          </div>
        );
      }
}