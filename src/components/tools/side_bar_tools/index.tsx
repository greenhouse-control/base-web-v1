import React, { Component } from 'react';
import { Layout, Menu, Icon } from 'antd';
import { Link } from "react-router-dom";
import 'antd/dist/antd.css';
const { Sider } = Layout;


export default class SideBarTools extends Component<{ match: any },{}> {
    state = {
        collapsed: false,
    };

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };
    render() {
        return (
            <div>
                <Sider theme="light" trigger={null} collapsible collapsed={this.state.collapsed}>
                    <Menu theme="light" mode="inline" defaultSelectedKeys={['1']}>
                        <Menu.Item style={{ color: '#135200' }}>
                            <Icon
                                className="trigger"
                                type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                                onClick={this.toggle}
                            />
                        </Menu.Item>

                        <Menu.Item key="1">
                            <Link to={`${this.props.match.url}/note`} style={{ color: '#096dd9' }} >
                                <Icon type="file-exclamation" />
                                <span>Note</span>
                            </Link>
                        </Menu.Item>

                        <Menu.Item key="2">
                            <Link to={`${this.props.match.url}/export`} style={{ color: '#08979c' }}>
                                <Icon type="export" />>
                                <span>Export</span>
                            </Link>
                        </Menu.Item>

                        <Menu.Item key="3">
                            <Link to={`${this.props.match.url}/setting`} style={{ color: '#531dab' }}>
                                <Icon type="setting" />
                                <span>Setting</span>
                            </Link>
                        </Menu.Item>
                    </Menu>
                </Sider>
            </div>
        )
    }

}
