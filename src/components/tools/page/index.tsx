import React, { Component } from 'react';
import { Layout } from 'antd';
import SideBarTools from '../side_bar_tools';
import ContentTools from '../content_tools';

export default class Tools extends Component<{ match: any }, {}> {
    state = {
        collapsed: false,
    };

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };

    render() {
        return (
            <div>
                <Layout>
                    <SideBarTools match={this.props.match}/>
                    <ContentTools match={this.props.match}/>
                </Layout>
            </div>
        );
    }
}