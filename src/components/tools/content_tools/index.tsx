import React from 'react';
import { Layout } from 'antd';
import { Route } from "react-router-dom";
import { Topic } from '../../tools/topic';

const { Content } = Layout;
export default class ContentHome extends React.Component<{match: any},{}> {
    render() {
        return (
            <Layout>
                <Content>
                    <div style={{ padding: 24, background: '#fff', height: '100%' }}>
                        <Route
                            path={`${this.props.match.url}/:topicId`}
                            component={Topic} />
                        <Route
                            exact
                            path={this.props.match.url}
                            render={() =>
                                <h3>
                                    Please select a topic.
                                        </h3>
                            }
                        />
                    </div>
                </Content>
            </Layout>
        )
    }

}
