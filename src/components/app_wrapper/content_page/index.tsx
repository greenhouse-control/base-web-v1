import React from 'react';
import { Layout} from 'antd';
import { Switch, Route} from "react-router-dom";
import Home from '../../home/page';
import Tools from '../../tools/page';
import About from '../../about/page';
 
const { Content } = Layout;
export default class MenuBar extends React.Component {
    render() {
        return (
            <Content
            style={{
                background: '#fff',
                padding: 24,
                margin: 0,
                minHeight: 280,
            }}
        >
            <Switch>
                <Route path="/home" component={Home} />
                <Route path="/tools" component={Tools} />
                <Route path="/about" component={About} />
            </Switch>
        </Content>
        )
    }

}
