import React from 'react';
import { BrowserRouter } from "react-router-dom";
import { Layout } from 'antd';
import 'antd/dist/antd.css';

import MenuBar from '../menu_bar';
import ContentPage from '../content_page';
export default class AppWrapper extends React.Component {
    state = {
        collapsed: false,
    };

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };

    render() {
        return (
            <BrowserRouter>
                <Layout>
                    
                    <Layout>
                        <MenuBar />
                    </Layout>
                    
                    <Layout>
                        <ContentPage />
                    </Layout>

                </Layout>
            </BrowserRouter>
        );
    }
}
