import React from 'react';
import { Menu } from 'antd';
import { Link } from "react-router-dom";

export default class MenuBar extends React.Component {
    render() {
        return (
            <Menu
                theme="light"
                mode="horizontal"
                defaultSelectedKeys={['1']}
                style={{ lineHeight: '48px', }}
            >
                <Menu.Item key="1"><Link to="/home">Home</Link></Menu.Item>
                <Menu.Item key="3"><Link to="/tools">Tools</Link></Menu.Item>
                <Menu.Item key="2"><Link to="/about">About</Link></Menu.Item>
            </Menu>
        )
    }

}
